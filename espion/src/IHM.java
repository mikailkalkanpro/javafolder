import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class IHM {

    String path = "/home/cd2gpa/dev/espion/data/data.csv";


    public static void main(String[] args) {


        IHM listEspion = new IHM();

        listEspion.ecrireCsv("\n3;Marion;Cotillard;marionC@gmail.com");
        listEspion.lireCsv();

        Scanner sc = new Scanner(System.in); //scanner tableau question


        System.out.println("Donner une chiffre entre 1 et 10 : ");

        String reponse;

        boolean condition = true; // condition de répetition

        do{
            reponse = sc.nextLine(); //recuperation de la reponsse

            if (!reponse.matches("-?\\w")){
                System.out.println("Vous devez entrez un chiffre");
            } else if (Integer.parseInt(reponse) <0 || Integer.parseInt(reponse) >10 ) {
                System.out.println("Vous deveza mettre un chiffre entre 1 et 10!!");
            } else if (Integer.parseInt(reponse) < choice) {
                System.out.println("C'est plus !");
            } else if (Integer.parseInt(reponse)> choice) {
                System.out.println("C'est moins !");
            }else {
                System.out.println("Gagné le chiffre etais " + choice);
                condition = false;
            }

        }while(condition);

    }

    public void lireCsv(){
        try {
            FileInputStream inputStream = new FileInputStream(this.path);

            Scanner sc = new Scanner(inputStream);

            String segment="-----------------";

            System.out.printf("+%s+%s+%s+%s+\n", segment,segment,segment,segment);
            while (sc.hasNext()){
                String line = sc.nextLine();

                String[] elems = line.split(";");

                System.out.printf("|%-10s|%-10s|%-10s|%-10s|\n", elems[0],elems[1],elems[2],elems[3]);
                System.out.printf("+%s+%s+%s+%s+\n", segment,segment,segment,segment);
            }

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void ecrireCsv(String line){
        try {
            File data = new File(this.path);
            FileWriter writer = new FileWriter(data, true);
            writer.write(line); // methiode ecriture ecrit le paramettre
            writer.flush(); // vide la memoir

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
