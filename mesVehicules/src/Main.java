import com.vehicules.Camion;
import com.vehicules.Vehicule;
import com.vehicules.Voiture;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Voiture voiture = new Voiture(); //constructeur voiture
        //voiture.marque = "renaul";  proprieter
        voiture.setMarque("Renaul");  //method
        voiture.setModel("zoe");
        voiture.setCarburation("électrique");
        voiture.setColors("red");
        voiture.setVitesseMax(130);
        voiture.setVitesseActuelle(0);

        System.out.println("voiture 1 " + voiture.toString());

        Voiture voiture2 = new Voiture();//constructeur voiture
        voiture2.setMarque("prout");
        voiture2.setModel("zoe");
        voiture2.setCarburation("électrique");
        voiture2.setColors("red");
        voiture2.setVitesseMax(130);
        voiture2.setVitesseActuelle(0);

        System.out.println("voiture 2 " + voiture2.toString());

        boolean equalOrFalse = voiture2 == voiture; // boolean objet == objet ? Pas meme objet meme si mem valeur

        System.out.println("Voiture identique ? " + equalOrFalse);


        boolean propertyEquality = voiture.equals(voiture2);

        System.out.println("Proprieter egal ? :" + propertyEquality);

        Voiture voitureRapide = new Voiture("rouge","380","ferari",280);// appel le constructeur




//        voiture.accelerer();

        voiture.setVitesseActuelle(50); //renaul voiture vitesse set a 50


//        voiture.freiner();

        voiture.mouvement(true, 15);//renaul voiture seter

        System.out.println(voiture.getColors());



        //STATIC

        System.out.println("nombre de roue : " + Voiture.nbrRoue);

        Camion benne = new Camion("Mercede", "benne","jaune");

        System.out.println(benne);

        Vehicule b1 = new Voiture("rouge","porche","911", 300);
        ((Voiture)b1).mouvement(true,190);

        Vehicule b2 = new Camion("vert","Audi","rodaster");

        System.out.println(b1.nbrRoue());
        System.out.println(b2.nbrRoue());


        ((Voiture)b1).vidanger();
        ((Camion)b2).vidanger();

    }
}