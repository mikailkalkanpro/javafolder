package com.vehicules;

import java.util.Objects;

public class Voiture extends Vehicule implements Videngeable {


    public Voiture() {
    }

    public Voiture(String colors, String model, String marque, int vitesse) {
        super();
        this.colors = colors;
        this.model = model;
        this.marque = marque;

    }

    public Voiture(String colors, String model, String marque, String carburation, int vitesseMax, int vitesseActuelle) {
        super();
        this.colors = colors;
        this.model = model;
        this.marque = marque;
        this.carburation = carburation;
        this.vitesseMax = vitesseMax;
        this.vitesseActuelle = vitesseActuelle;
    }



    private void accelerer(int vitesse){
        this.vitesseActuelle += vitesse;
        vitesseActuelle(vitesse);
    }

    private void freiner(int vitesse){
        if (this.vitesseActuelle >9) {
            this.vitesseActuelle -= vitesse;
            vitesseActuelle(vitesse);
        }
    }

    private void vitesseActuelle(int vitesse){
        this.vitesseActuelle = vitesse;
        System.out.println("La voiture "+ this.marque + " va à : "+ this.vitesseActuelle + "km/h");
    }

    public void setVitesseMax(){
        System.out.println("La voiture "+ this.marque + " peut rouler jusqua : "+ this.vitesseMax + "km/h");
    }

    /**
     *
     * @param direction utilise true pour avancer
     * @param vitesse  valeur modifier la vitesse
     */
    public void mouvement(boolean direction, int vitesse) {
        if (direction == true){
            this.accelerer(vitesse); //appel de la methode interne
        }else{
            this.freiner(vitesse);
        }
    }




    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getModel() {
        return model;
    }



    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getCarburation() {
        return carburation;
    }

    public void setCarburation(String carburation) {
        this.carburation = carburation;
    }

    public int getVitesseMax() {
        return vitesseMax;
    }

    public void setVitesseMax(int vitesseMax) {
        this.vitesseMax = vitesseMax;
    }

    public int getVitesseActuelle() {
        return vitesseActuelle;
    }

    public void setVitesseActuelle(int vitesseActuelle) {
        this.vitesseActuelle = vitesseActuelle;
    }

    @Override
    public void vidanger() {

    }

    @Override
    public void metDeLHuile() {

    }
}
