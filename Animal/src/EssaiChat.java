import java.util.Random;

public class EssaiChat {
    static Random alea= new Random(); //method static random

    static Animal tirage(){
        return (Math.abs(alea.nextInt()) %2 == 0 ? new Chien():new Chat() ); //modulo du tirage reste next int 0.1 0.99

        // return alea.nextBoolean() ? new Chien():new Chat();

    }
    public static void main(String[] args) {
        tirage().action();
    }
}
