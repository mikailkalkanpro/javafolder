import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class JSONManage {

    ///home/cd2gpa/Documents/Java/JSONTraitement/data/myJson.json
    private String path ="/home/cd2gpa/dev/JSONTraitement/data/myJson.json";
    public static void main(String[] args) {
        JSONManage jsm = new JSONManage();
        Personne personne = new Personne("TaTO",0514615130);
        jsm.ecrireJson(personne);
        jsm.afficheJson();
    }


    public JSONArray afficheJson(){
        JSONArray json = this.lireJson();
        return json;
    }
    //lecture du json
    public JSONArray lireJson(){
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        try {
            FileReader fr = new FileReader(this.path);
            Object ob = parser.parse(fr);//parsing du json
            System.out.println("ob = "+ob);
            jsonArray = (JSONArray) ob; // on cast en JsonArray objet parser
            return jsonArray;
//            jsonArray.forEach(
//                    contact -> {
//                        JSONObject myContact = (JSONObject) contact;
//                        System.out.println(myContact);
//                        JSONObject personne =(JSONObject) myContact.get("contact");
//                        System.out.println(personne);
//                    }
//            );
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (ParseException e) {
            return jsonArray;
            //throw new RuntimeException(e.getMessage());
        }

        return jsonArray;
    }

    //ecriture du Json
    public void ecrireJson(Personne p1){
        try{
//            JSONArray rootContact = new JSONArray();//construction du tableau json qui vas contenir le list
            JSONArray rootContact = this.afficheJson();//construction du tableau json qui vas contenir le list
            rootContact.add(p1.toJson());
            FileWriter fw = new FileWriter(this.path);//je prépare l'écriture du fichier
            fw.write(rootContact.toJSONString());//j'écris dans le fichier grace au json array transformer en string
            fw.flush();//je vide mon objet
        }catch (IOException e){
            throw new RuntimeException(e.getMessage());
        }
    }

}