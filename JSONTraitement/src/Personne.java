import org.json.simple.JSONObject;

public class Personne {

    private String name;
    private int  tel;

    public Personne(){}

    public Personne(String name, int tel) {
        this.name = name;
        this.tel = tel;
    }

    public JSONObject toJson(){
        JSONObject contact = new JSONObject(); // creation de l'objet json de contact
        contact.put("name", this.name); // ajout de proprieter et de valeur
        contact.put("tel", this.tel);
        JSONObject contactList = new JSONObject();//create d'un objet json de container
        contactList.put("contacts",contact);

        return contactList;
    }
}
