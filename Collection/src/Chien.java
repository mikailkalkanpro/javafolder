public class Chien {
    public String nom;
    public String race;
    public String couleur;
    public int age;

    public Chien() {
    }

    public Chien(String nom, String race, String couleur, int age) {
        this.nom = nom;
        this.race = race;
        this.couleur = couleur;
        this.age = age;
    }
}
