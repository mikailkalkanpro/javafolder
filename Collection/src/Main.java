import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        List<Chien> chenil = new ArrayList<Chien>(); // list array list egal listiterator

        chenil.add(new Chien("rex", "berger","ref",12));
        chenil.add(new Chien("atila", "rot","ref",5));
        chenil.add(new Chien("medor", "buldog","ref",7));
        chenil.add(new Chien("filou", "berger","ref",3));


        ListIterator iter = chenil.listIterator();

        while(iter.hasNext()){
            Chien chien = (Chien) iter.next();
            System.out.println(chien.nom + " est a l'index " + (iter.nextIndex()-1) + "et il a " + chien.age + "ans");
        }


        Set<String> ville = new HashSet<String>(); //set egal iterator
        ville.add("Perpignan");
        ville.add("Paris");
        ville.add("Bordeaux");
        ville.add("Lille");

        Iterator iter2 = ville.iterator();

        while(iter2.hasNext()){
            System.out.println("Ville qui a un chenile " + iter2.next());
        }


        TreeSet<String> mois = new TreeSet<String>(); //triage list iterator normal
        mois.add("Avril");
        mois.add("Mais");
        mois.add("Juin");
        mois.add("Janvier");
        mois.add("Septembre");

        Iterator iterm = mois.iterator();

        while (iterm.hasNext()){
            System.out.println(iterm.next());
        }

        HashMap<Integer,String> map = new HashMap<Integer, String>();

        map.put(1,"histoir");
        map.put(2,"Français");
        map.put(3,"Geo");
        map.put(4,"Picine");
        map.put(5,"Dev");

        for (Integer res: map.keySet()){// keySet array keys  cles valeur
            System.out.println(res + " " + map.get(res));
        }

        Voiture voiture = new Voiture();
        voiture.typeBoite = TBV.MANUAL;
    }
}