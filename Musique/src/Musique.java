import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

public class Musique{

    static JFrame frame = new JFrame("Musique jolie");
    static MyDrawPanel mdp;

    public static void main(String[] args) {
        Musique m = new Musique();
        m.play();
    }

    public void setUpGui(){
        mdp = new MyDrawPanel(); // methode de config
        frame.setContentPane(mdp);
        frame.setBounds(300,300,500,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    public  void play(){
        setUpGui();
        try {
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            Sequence sequence =  new Sequence(Sequence.PPQ, 4);
            Track track = sequence.createTrack();
            int[] evenement = {127};
            sequencer.addControllerEventListener(mdp,evenement);
            int rm = 0;

            for(int i = 5; i < 60;i+=4){
                rm = (int) ((Math.random()*50) +1); // transform en int le random  +1 pour pas x 0
                track.add(makeEvent(144,1,rm,100,i));
                track.add(makeEvent(176,1,127,100,i));
                track.add(makeEvent(128,1,rm,100,2));
                //System.out.println(i);
            }
            //première note start
//            ShortMessage a = new ShortMessage();
//            a.setMessage(144,1,38,100);
//            MidiEvent noteOn = new MidiEvent(a, 1);
//            track.add(noteOn);
//
//            ShortMessage b = new ShortMessage();
//            b.setMessage(128, 1, 38, 100);
//            MidiEvent noteOff = new MidiEvent(b,16);
//            track.add(noteOff);
//
//            ShortMessage a1 = new ShortMessage();
//            a1.setMessage(144,1,38,100);
//            MidiEvent noteOn1 = new MidiEvent(a, 1);
//            track.add(noteOn1);
//
//
//            ShortMessage b1 = new ShortMessage();
//            b1.setMessage(128, 1, 58, 100);
//            MidiEvent noteOff1 = new MidiEvent(b,16);
//            track.add(noteOff1);


            sequencer.setSequence(sequence);
            sequencer.setTempoInBPM(220);
            sequencer.start();
            System.out.println("on fait de la musique");
        }catch(MidiUnavailableException | InvalidMidiDataException e){
            System.out.println("erreur : " + e.getMessage());
        }
    }
    private MidiEvent makeEvent(int cmd, int ch, int one, int two, int tick){
        MidiEvent event = null;
        try {
            ShortMessage msg = new ShortMessage();
            msg.setMessage(cmd, ch, one,two);
            event = new MidiEvent(msg, tick);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return event ;
    }

//    @Override
//    public void controlChange(ShortMessage event){
//        System.out.println("la");
//    }

    private static class MyDrawPanel extends JPanel implements ControllerEventListener{
        boolean msg = false;
        @Override
        public void controlChange(ShortMessage event) {

            msg = true;
            repaint();
        }

        public void paintComponent(Graphics graphics){
            System.out.println(msg);
            if (msg){
                //tirage au sort des couleurs
                int red = (int) (Math.random() * 255);
                int green = (int) (Math.random() * 255);
                int blue = (int) (Math.random() * 255);
                //affectation de la couleur au graphique
                graphics.setColor(new Color(red, green, blue));
                //définition du graphique
                int ht = (int) ((Math.random()*120)+10);
                int wd = (int) ((Math.random()*120)+10);
                int x = (int) ((Math.random()*40)+10);
                int y = (int) ((Math.random()*40)+10);
                //construction du rectangle
                graphics.fillRect(x,y,wd,ht);
                msg = false;
            }
        }
    }
}