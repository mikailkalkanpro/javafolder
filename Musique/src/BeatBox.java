import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ListIterator;

public class BeatBox {

    JFrame theFrame;
    JPanel mainPanel;
    Box nameBox;
    JComboBox<String> chan;
    ArrayList<JCheckBox> checkBoxesListe;

    Sequencer sequencer;
    Sequence seq;
    Track track;


    String[] instrumentNames = { "Bass Drum", "Closed hi-hat", "Open hi-hat", "Accoustic snare", "Crash symbal",
            "Hand Clap", "High tom", "Hi bongo", "Maracas", "Whistle", "Low conga", "Cow bell", "Vibraslap",
            "Low mid-tom", "High agogo", "Open hi conga" };

    int[] instruments = { 35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58, 47, 67, 63 };

    Synthesizer synthesizer;
    Instrument[] instrumentals;
    String[] chanels = new String[128];

    public static void main(String[] args) {
        new BeatBox().buildGui();

    }

    public BeatBox() {
        try{
            synthesizer = MidiSystem.getSynthesizer();
            synthesizer.open();
            instrumentals = synthesizer.getAvailableInstruments();
            for (int i = 0; i < 128; i++) {
                chanels[i] = instrumentals[i].getName() + i;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    //methode qui construise l'interface en faisant la frame definissant la taille et la position
    //ajouter 5 boutons starts, stop, acceller, ralentir, décocher tout,

    private  void buildGui(){
        checkBoxesListe = new ArrayList<JCheckBox>();
        //Frame
        theFrame = new JFrame("BeatBox");
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout layout = new BorderLayout();
        JPanel backGround = new JPanel(layout);

        backGround.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        Box buttonBox = new Box(BoxLayout.Y_AXIS);

        //Bouton
        JButton btnStart = new JButton("Start");
        btnStart.setMargin(new Insets(5,5,5,5));

        JButton btnStop = new JButton("Stop");
        btnStop.setMargin(new Insets(5,5,5,5));

        JButton btnAccelere = new JButton("Accelére");
        btnAccelere.setMargin(new Insets(5,5,5,5));

        JButton btnRalenti = new JButton("Ralenti");
        btnRalenti.setMargin(new Insets(5,5,5,5));

        JButton btnDecocheTout = new JButton("Décoche tout");
        btnDecocheTout.setMargin(new Insets(5,5,5,5));


        //Grille
        GridLayout grid = new GridLayout(16,16);
        grid.setVgap(1);
        grid.setHgap(2);
        //Panel
        mainPanel = new JPanel(grid);

        //ajout des checkBox
        for (int i = 0; i < 256; i++) {
            JCheckBox c = new JCheckBox();
            c.setSelected(false);
            checkBoxesListe.add(c);
            mainPanel.add(c);
        }

        chan = new JComboBox<String>(chanels);




        // add des boutons a la buttonBox
        buttonBox.add(btnStart);
        buttonBox.add(btnStop);
        buttonBox.add(btnAccelere);
        buttonBox.add(btnRalenti);
        buttonBox.add(btnDecocheTout);
        buttonBox.add(chan);

        //add les listener


        //appel des listeners de bouton
        btnStart.addActionListener(new MyStartListener());
        btnStop.addActionListener(new MyStopListener());
        btnAccelere.addActionListener(new MyAccelererListener());
        btnRalenti.addActionListener(new MyRalentirListener());
        btnDecocheTout.addActionListener(new MyDecocheAllListener());
        chan.addActionListener(new MyChangeListener());



        //creation de la box avec nom instruments
        nameBox = new Box(BoxLayout.Y_AXIS);
        for (int i = 0; i < 16; i++) {
            nameBox.add(new JLabel(instrumentNames[i]));
        }

        backGround.add(BorderLayout.EAST, buttonBox);
        backGround.add(BorderLayout.WEST,nameBox);
        backGround.add(BorderLayout.CENTER, mainPanel);

        //initialisation du séquencer
        setupMidi();

        theFrame.getContentPane().add(backGround);
        theFrame.setBounds(100,100,300,300);
        theFrame.pack();//permet de changer la taille en fonction du contenu
        theFrame.setVisible(true);
    }

    public void buildTrackAndStart(int chanelChoice){
        int [] trackList = null;
        seq.deleteTrack(track);
        track = seq.createTrack();
        track.add(makEvent(192,1,chanelChoice, 0,15));

        for (int i = 0; i < 16; i++) {
            trackList = new int[16];
            int key = instruments[i];
            for (int j = 0; j < 16; j++) {
                JCheckBox jc = checkBoxesListe.get( j + 16*i );
                if (jc.isSelected()) {
                    trackList[j] = key;
                }else {
                    trackList[j] = 0;
                }

            }
            makeTrack(trackList);
            track.add(makEvent(176,1,127,0,16));
        }
        try {
            sequencer.setSequence(seq);
            sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);
            sequencer.start();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void makeTrack(int[] trackList) {
        for (int i = 0; i < 16; i++) {
            int key = trackList[i];
            if (key != 0) {
                track.add(makEvent(144,1,key,100,i)); //note ouverte
                track.add(makEvent(128,1,key,100,i+2)); // note fermer
            }
        }
    }

    private void setupMidi(){

        try{
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            seq = new Sequence(Sequence.PPQ,4);
            track = seq.createTrack();
            sequencer.setTempoInBPM(200);
        }catch (Exception e){}
    }

    private  MidiEvent makEvent(int cmd, int ch, int data1, int data2, int tick){
        MidiEvent event = null;

        try {
            ShortMessage message = new ShortMessage();
            message.setMessage(cmd, ch,data1,data2);
            event = new MidiEvent(message,tick);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return event;
    }

    //    region Listener
    public class MyStartListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            buildTrackAndStart(chan.getSelectedIndex());
            System.out.println("Marche " + chan.getSelectedIndex());
        }
    }

    private class MyStopListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            sequencer.stop();
            System.out.println("Stop");
        }
    }

    private class MyDecocheAllListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            sequencer.stop();

            for (int i = 0; i < 256; i++) {
                JCheckBox c = checkBoxesListe.get(i);
                c.setSelected(false);
            }
//             MArche pas
//            ListIterator iter = (ListIterator) checkBoxesListe.iterator();
//            while(iter.hasNext()){
//                JCheckBox jc = (JCheckBox) iter.next();
//                jc.setSelected(false);
//            }
        }
    }

    private class MyAccelererListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            float tempo = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float)(tempo * 1.15));
        }
    }

    private class MyRalentirListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            float tempo = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float)(tempo * 0.95));
//            BPM -=20;
//            sequencer.setTempoInBPM(BPM);
        }
    }

    private class MyChangeListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            sequencer.stop();
            JComboBox<String> cb =(JComboBox<String>) e.getSource();
            int ch = cb.getSelectedIndex();
            buildTrackAndStart(ch);
        }
    }
//    endregion
}