import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleGui {

    JLabel label;
    JFrame frame;

    public static void main(String[] args) {
        SimpleGui gui = new SimpleGui();
        gui.openWindow();
    }

    public void openWindow() {
        frame = new JFrame("Ma premièrre fenêtre");

        JPanel panelMenu = new JPanel();
        panelMenu.setBackground(Color.LIGHT_GRAY);
        panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.X_AXIS));

        JButton btn1 = new JButton("Hey you");
        btn1.addActionListener(new ChangLabel());

        JButton btn2 = new JButton("Hey you 2");
        btn2.addActionListener(new ChargeForm());

        panelMenu.add(btn1);
        panelMenu.add(btn2);

        label = new JLabel();
        label.setText("Welcome to the jungle");
        label.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel label2 = new JLabel();
        label2.setText("Info");

//        JLabel labelTest = new JLabel();
//        labelTest.setText("Ceci est un test");
//        labelTest.setForeground(Color.BLACK);

//        panelMenu.add(labelTest);

        frame.getContentPane().add(BorderLayout.NORTH, panelMenu);
        frame.getContentPane().add(BorderLayout.CENTER, label);
        frame.getContentPane().add(BorderLayout.WEST, label2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    private class ChangLabel implements ActionListener {

        int nbrClique = 0;

        @Override
        public void actionPerformed(ActionEvent e) {
            label.setText("j'ai cliqué " + (nbrClique++) + " fois");
        }
    }

    private class ChargeForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            buildForm();
        }

        private void buildForm() {
            JPanel formPanel = new JPanel();
            formPanel.setPreferredSize(new Dimension(300, 500));

            //defenition du layout
            Box formLayout = new Box(BoxLayout.Y_AXIS);

            //Defenir les champ de formulaire;

            JTextField nom = new JTextField(10);
            JTextField prenom = new JTextField(10);
            JTextField age = new JTextField(10);

            //Ajout des champs au layout
            formLayout.add(nom);
            formLayout.add(prenom);
            formLayout.add(age);

            //ajout du layout au panel
            formPanel.add(formLayout);

            frame.getContentPane().add(BorderLayout.EAST, formPanel);

            formPanel.setBackground(Color.gray);

            //Redessiner la frame
            frame.revalidate();
        }
    }
}
